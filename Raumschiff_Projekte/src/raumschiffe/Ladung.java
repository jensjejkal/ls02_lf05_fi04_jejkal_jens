package raumschiffe;

/**
 * Ladungsklasse
 * @author Jens Jejkal
 * @version 1.0
 * 03.03.2021
 */

public class Ladung {
	
	private String art;
	private int anzahl;
	
	/**
	 * parameterloser Konstruktor f�r die Ladungsklasse
	 */
	public Ladung() {
		
	}
	
	/**
	 * Konstruktor f�r die Ladungsklasse mit den Parametern:
	 * @param art - Ladungsbezeichnung
	 * @param anzahl - Ladungsanzahl
	 */
	public Ladung(String art, int anzahl) {
		setArt(art);
		setAnzahl(anzahl);
	}
	
	/**
	 * Ausgabe der Ladungsparametern als seperate Strings:
	 * @param art - Ladungsbezeichnung
	 * @param anzahl - Ladungsanzahl
	 */
	public void getLadung(String art, int anzahl) {
		System.out.printf("Ladungsart: %s", art);
		System.out.printf("Anzahl: %d", anzahl);
	}
	
	/**
	 * Getter der Art der Ladung
	 * @return art - Ladungsbezeichnung
	 */
	public String getArt() {
		return art;
	}
	
	/**
	 * Setter der Art der Ladung:
	 * @param art - Ladungsbezeichnung
	 */
	public void setArt(String art) {
		this.art = art;
	}
	
	/**
	 * Getter der Anzahl der Ladung
	 * @return anzahl - Ladungsanzahl
	 */
	public int getAnzahl() {
		return anzahl;
	}
	
	/**
	 * Setter der Anzahl der Ladung:
	 * @param anzahl - Ladungsanzahl
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	
	
	
}
