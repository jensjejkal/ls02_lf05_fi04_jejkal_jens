package raumschiffe;

import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;

/**
 * Raumschiffklasse
 * @author Jens Jejkal
 * @version 1.0
 * 03.03.2021
 *
 */

public class Raumschiff {
	
	private String name;
	private int energieVersorgung;
	private ArrayList<String> logbuchEintraege = new ArrayList<String>();
	private int trefferAnzahl;
	private int statusLebenserhaltungsSystem;
	private int statusSchutzschild;
	private int statusHuelle;
	private int anzahlPhotonentorpedos;
	private int anzahlAndroiden;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungen = new ArrayList<Ladung>();
	
	/**
	 * parameterloser Konstruktor f�r die Raumschiffsklasse
	 */
	public Raumschiff() {
		
	}	
	
	/**
	 * Konstruktor f�r die Raumschiffsklasse mit folgenden Parametern:
	 * @param name - Name des Raumschiffes
	 * @param energieVersorgung - Prozent der Energieversorgung des Raumschiffes
	 * @param statusLebenserhaltungsSystem - Prozent des Lebenserhaltungssystems
	 * @param statusSchutzschild - Prozent des Schutzschildes
	 * @param statusHuelle - Prozent der H�lle
	 * @param anzahlPhotonentorpedoes - Anzahl der Photonentorpedoes auf dem Raumschiff
	 * @param anzahlAndroiden - Anzahl der Androiden auf dem Raumschiff
	 */
	public Raumschiff(String name, int energieVersorgung, int statusLebenserhaltungsSystem, int statusSchutzschild, int statusHuelle, int anzahlPhotonentorpedoes, int anzahlAndroiden) {
		setName(name);
		setEnergieVersorgung(energieVersorgung);
		setStatusLebenserhaltungsSystem(statusLebenserhaltungsSystem);
		setStatusSchutzschild(statusSchutzschild);
		setStatusHuelle(statusHuelle);
		setAnzahlPhotonentorpedos(anzahlPhotonentorpedos);
		setAnzahlAndroiden(anzahlAndroiden);
	}
	
	/**
	 * Funktion zur Anzeige des Zustandes des Raumschiffes.
	 * Keine Eingabe erforderlich.
	 * Ausgabe: Raumschiffname, Prozent der Energieversorgung, Prozent des Lebenserhaltungssystem, Prozent des Schutzschildes, Prozent der H�lle, vorhandene Ladungen auf dem Raumschiff
	 */
	public void zustandAnzeigen() {
		System.out.println();
		System.out.printf("Raumschiffstatus f�r das Raumschiff: %s\n", this.name);
		System.out.printf("Energieversorgung: %d\n", this.energieVersorgung);
		System.out.printf("Lebenserhaltungssystem: %d\n", this.statusLebenserhaltungsSystem);
		System.out.printf("Schutzschild: %d\n", this.statusSchutzschild);
		System.out.printf("H�lle: %d\n", this.statusHuelle);
		System.out.println();
		System.out.printf("Ladungen vom Raumschiff: %s\n", this.name);
		
		for(int i = 0; i < ladungen.size(); i++) {
			System.out.printf("%s: %d", this.ladungen.get(i).getArt(), this.ladungen.get(i).getAnzahl());
			System.out.println();
		}
		System.out.println();
	}
		
	/**
	 * Getter des Raumschiffnamens:
	 * @return Raumschiffname
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter des Raumschiffnamens:
	 * @param name - Raumschiffname
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter der Energieversorgung:
	 * @return Prozent der Energieversorgung
	 */
	public int getEnergieVersorgung() {
		return energieVersorgung;
	}
	
	/**
	 * Setter der Energieversorgung:
	 * @param energieVersorgung - Prozent der Energieversorgung
	 */
	public void setEnergieVersorgung(int energieVersorgung) {
		this.energieVersorgung = energieVersorgung;
	}
	
	/**
	 * Getter der Logbucheintr�ge:
	 * @return Logbucheintr�ge
	 */
	public ArrayList<String> getLogbuchEintraege() {
		return logbuchEintraege;
	}
	
	/**
	 * Setter der Logbucheintr�ge:
	 * @param logbuchEintraege - Logbucheintr�ge 
	 */
	public void setLogbuchEintraege(ArrayList<String> logbuchEintraege) {
		this.logbuchEintraege = logbuchEintraege;
	}
	
	/**
	 * Getter der Trefferanzahl:
	 * @return Anzahl der Treffer
	 */
	public int getTrefferAnzahl() {
		return trefferAnzahl;
	}
	
	/**
	 * Setter der Trefferanzahl
	 * @param trefferAnzahl - Anzahl der Treffer
	 */
	public void setTrefferAnzahl(int trefferAnzahl) {
		this.trefferAnzahl = trefferAnzahl;
	}
	
	/**
	 * Getter des Status des Lebenserhaltungssystems
	 * @return Prozent des Lebenserhaltungssystems
	 */
	public int getStatusLebenserhaltungsSystem() {
		return statusLebenserhaltungsSystem;
	}
	
	/**
	 * Setter des Status des Lebenserhaltungssystems
	 * @param statusLebenserhaltungsSystem - Prozent des Lebenserhaltungssystems
	 */
	public void setStatusLebenserhaltungsSystem(int statusLebenserhaltungsSystem) {
		this.statusLebenserhaltungsSystem = statusLebenserhaltungsSystem;
	}
	
	/**
	 * Getter des Status des Schutzschildes
	 * @return Prozent des Schutzschildes
	 */
	public int getStatusSchutzschild() {
		return statusSchutzschild;
	}
	
	/**
	 * Setter des Status des Schutzschildes
	 * @param statusSchutzschild - Prozent des Schutzschildes
	 */
	public void setStatusSchutzschild(int statusSchutzschild) {
		this.statusSchutzschild = statusSchutzschild;
	}
	
	/**
	 * Getter des Status der H�lle
	 * @return Prozent der H�lle
	 */
	public int getStatusHuelle() {
		return statusHuelle;
	}
	
	/**
	 * Setter des Status der H�lle
	 * @param statusHuelle - Prozent der H�lle
	 */
	public void setStatusHuelle(int statusHuelle) {
		this.statusHuelle = statusHuelle;
	}
	
	/**
	 * Getter der Anzahl der Photonentorpedos auf dem Raumschiff
	 * @return Anzahl der Photonentorpedos
	 */
	public int getAnzahlPhotonentorpedos() {
		return anzahlPhotonentorpedos;
	}
	
	/**
	 * Setter der Anzahl der Photonentorpedos auf dem Raumschiff
	 * @param anzahlPhotonentorpedos - Anzahl der Photonentorpedos
	 */
	public void setAnzahlPhotonentorpedos(int anzahlPhotonentorpedos) {
		this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
	}
	
	/**
	 * Getter der Anzahl der Androiden auf dem Raumschiff
	 * @return Anzahl der Androiden
	 */
	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}
	
	/**
	 * Setter der Anzahl der Androiden auf dem Raumschiff
	 * @param anzahlAndroiden - Anzahl der Androiden
	 */
	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}
	
	/**
	 * Getter des Broadcastkommunikators
	 * @return Broadcastkommunikator
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	/**
	 * Setter des Broadcastkommunikators
	 * @param broadcastKommunikator - Broadcastkommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}
	
	/**
	 * Funktion f�r das Hinzuf�gen einer Ladung zum Raumschiff
	 * @param l - Ladung
	 */
	public void ladungHinzufuegen(Ladung l) {
		this.ladungen.add(l);
	}
	
	/**
	 * Getter f�r das Ladungsverzeichnis
	 * @return Ladungsverzeichnis
	 */
	public ArrayList<Ladung> getLadungen(){
		return this.ladungen;
	}
	
	/**
	 * Funktion f�r das Aufr�umen des Ladungsverzeichnis.
	 * Kein Parameter wird ben�tigt.
	 */
	public void aufrauemenLadungsverzeichnis() {
		for(int i = 0; i < this.ladungen.size(); i++) {
			if(this.ladungen.get(i).getAnzahl() == 0) {
				this.ladungen.remove(i);
			}
		}
	}
	
	/**
	 * Funktion f�r das Abschiessen von Photonentorpedoes von einem Raumschiff auf ein anderes Raumschiff
	 * @param anzahl - Anzahl der Photonentorpedoes, die abgeschossen werden sollen 
	 * @param schiff - Raumschiff, das getroffen werden soll
	 */
	public void photonenTorpedoesAbschiessen(int anzahl, Raumschiff schiff) {
		boolean gefunden = false;
		int i = 0;
		while(gefunden == false && i < this.ladungen.size()){
			String artLadung = this.ladungen.get(i).getArt();
			int anzahlLadung = this.ladungen.get(i).getAnzahl(); 
			if(artLadung == "Photonentorpedos" && anzahlLadung > 0) {
				anzahlPhotonentorpedos = this.ladungen.get(i).getAnzahl() + anzahlPhotonentorpedos;
				this.ladungen.get(i).setAnzahl(0);
				gefunden = true;
			}
			else {
				i++;
			}
		}
		
		if(this.anzahlPhotonentorpedos <= 0) {
			System.out.println("Keine Photonentorpedoes vorhanden");
			while(anzahl > 0) {
				nachrichtSenden("-=*Click*=-");
				anzahl = anzahl - 1;
			}
			
			}
		else if (anzahl > this.anzahlPhotonentorpedos) {
			System.out.printf("%d Photonentorpedos eingesetzt\n", this.anzahlPhotonentorpedos);
			while(this.anzahlPhotonentorpedos > 0) {
				angriffRegistieren(schiff);
				this.anzahlPhotonentorpedos = this.anzahlPhotonentorpedos - 1;
			}
			
		}
		else {
			setAnzahlPhotonentorpedos(this.anzahlPhotonentorpedos - anzahl);
			System.out.printf("%d Photonentorpedos eingesetzt\n", anzahl);
			while(anzahl > 0) {
				angriffRegistieren(schiff);
				anzahl = anzahl - 1;
				this.anzahlPhotonentorpedos = this.anzahlPhotonentorpedos - 1; 
			}
			
		}
	}
	
	/**
	 * Funktion f�r das Abschiessen der Phaserkanone auf ein Raumschiff
	 * @param schiff - Raumschiff, auf welches die Phaserkanone schiesst 
	 */
	public void phaserKanoneAbschiessen(Raumschiff schiff) {
		if(this.energieVersorgung < 50) {
			nachrichtSenden("-=*Click*=-");
		}
		else {
			energieVersorgung = this.energieVersorgung - 50;
			nachrichtSenden("Phaserkanone abgeschossen");
			angriffRegistieren(schiff);
		}
	}
	
	/**
	 * Funktion zur Verarbeitung eines Treffers
	 * @param Schiff - Raumschiff, welches getroffen wurde
	 */
	private void angriffRegistieren(Raumschiff Schiff) {
		System.out.printf("%s wurde getroffen\n", Schiff.name);
		if(Schiff.statusSchutzschild > 0) {
			Schiff.statusSchutzschild = Schiff.statusSchutzschild - 50;
			if(Schiff.statusSchutzschild <= 0) {
				Schiff.statusHuelle = Schiff.statusHuelle - 50;
				Schiff.energieVersorgung = Schiff.energieVersorgung - 50;
					if(Schiff.statusHuelle <= 0) {
						Schiff.statusLebenserhaltungsSystem = 0;
						nachrichtSenden("Lebenserhaltungssysteme wurden zerst�rt");
					}	
			}
		}
	}
	
	/**
	 * Funktion zum Senden eines Reparaturauftrages an die Androiden des Raumschiffes
	 * @param schutzSchild - Soll das Schutzschild repariert werden?
	 * @param huelle - Soll die H�lle repariert werden?
	 * @param leben - Soll das Lebenserhaltungssystem repariert werden?
	 * @param anzahlA - Wie viele Androiden sollen rausgesendet werden?
	 */
	public void reparaturAuftragSenden(boolean schutzSchild, boolean huelle, boolean leben, int anzahlA) {
		Random zufallsGenerator = new Random();
		int zahl = zufallsGenerator.nextInt(101);
		int ergebnis = 0;
		int anzahlBoolean = 0;
		
		if(anzahlA > this.anzahlAndroiden) {
			anzahlA = this.anzahlAndroiden;
		}
		
		if(schutzSchild) {
			anzahlBoolean = anzahlBoolean + 1;
		}
		if(huelle) {
			anzahlBoolean = anzahlBoolean + 1;
		}
		if(leben) {
			anzahlBoolean = anzahlBoolean + 1;
		}
		
		ergebnis = (zahl * Math.abs(anzahlA)) / Math.abs(anzahlBoolean);
			
		if(schutzSchild) {
			this.statusSchutzschild = this.statusSchutzschild + ergebnis;
		}
		if(huelle) {
			this.statusHuelle = this.statusHuelle + ergebnis;
		}
		if(leben) {
			this.statusLebenserhaltungsSystem = this.statusLebenserhaltungsSystem + ergebnis;
		}
			
	} 
	
	/**
	 * Funktion f�r das Senden einer Nachricht und Ausgabe der Nachricht in der Konsole
	 * @param nachricht - Inhalt der Nachricht
	 */
	public void nachrichtSenden(String nachricht) {
		Raumschiff.broadcastKommunikator.add(nachricht);
		this.logbuchEintraege.add(nachricht);
		System.out.println(nachricht);
	}
	
}
