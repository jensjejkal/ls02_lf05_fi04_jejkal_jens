package raumschiffe;

/**
 * Raumschifftest - Krieg der Raumschiffe.
 * <br>Kampf zwischen Klingonen, Romulaner und Vulkanier.
 * @author Jens Jejkal
 * 04.03.2021
 */
public class RaumschiffTest {
	/**
	 * Erstellung der Raumschiffobjekte und Ladungen der Einzelnen Raumschiffe.
	 * <br> Ladungen werden den Raumschiffen hinzugef�gt.
	 * <br> Klingonen schie�en Photonentorpedos auf Romulaner.
	 * <br> Romulaner schie�en Phaserkanonen zur�ck.
	 * <br> Vulkanier senden eine Nachricht raus und reparieren das Schiff.
	 * <br> Klingonen rufen den Status des Schiffes ab.
	 * <br> Vulkanier laden Photonentorpedos und r�umen ihren Ladungsverzeichnis auf.
	 * <br> Klingonen versuchen nochmal auf die Romulaner zu schie�en.
	 * <br> Am Ende rufen alle Schiffe den Status ab.
	 * @param args
	 */
	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 100, 80, 50, 0, 5);
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Kligonen Schwert", 200);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedos", 3);
		
		klingonen.ladungHinzufuegen(l1);
		klingonen.ladungHinzufuegen(l2);
		romulaner.ladungHinzufuegen(l3);
		romulaner.ladungHinzufuegen(l4);
		romulaner.ladungHinzufuegen(l5);
		vulkanier.ladungHinzufuegen(l6);
		vulkanier.ladungHinzufuegen(l7);
		
		klingonen.photonenTorpedoesAbschiessen(1, romulaner);
		romulaner.phaserKanoneAbschiessen(klingonen);
		vulkanier.nachrichtSenden("Gewalt ist nicht logisch.");
		klingonen.zustandAnzeigen();
		vulkanier.reparaturAuftragSenden(true, true, false, 5);
		vulkanier.setAnzahlPhotonentorpedos(l7.getAnzahl() + vulkanier.getAnzahlPhotonentorpedos());
		l7.setAnzahl(0);
		vulkanier.aufrauemenLadungsverzeichnis();
		klingonen.photonenTorpedoesAbschiessen(2, romulaner);
		klingonen.zustandAnzeigen();
		romulaner.zustandAnzeigen();
		vulkanier.zustandAnzeigen();
	}

}
