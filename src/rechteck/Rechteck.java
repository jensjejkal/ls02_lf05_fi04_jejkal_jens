package rechteck;

public class Rechteck {
	
	private double lengthA;
	private double lengthB;
	
	public Rechteck(double lengthA, double lengthB){
		setLengthA(lengthA);
		setLengthB(lengthB);
	}
	
	public void setLengthA(double lengthA) {
		if(lengthA > 0)
			this.lengthA = lengthA;
		else
			this.lengthA = 0;
	}
	
	public double getLengthA() {
		return this.lengthA;
	}

	public void setLengthB(double lengthB) {
		if(lengthB > 0)
			this.lengthB = lengthB;
		else
			this.lengthB = 0;
	}
	
	public double getLengthB() {
		return this.lengthB;
	}
	
	public double getUmfang() {
		return 2*lengthA + 2*lengthB;
	}
	
	public double getFlaeche() {
		return lengthA * lengthB;
	}
	
	public double getDurchmesser() {
		double c;
		c = lengthA * lengthA + lengthB * lengthB;
		return Math.sqrt(c);
	}
	
}
